<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link             
 * @since             1.0.0
 * @package           thf_donate
 *
 * @wordpress-plugin
 * Plugin Name:       Donator THF
 * Plugin URI:        https://.com/
 * Description:       Donator THF
 * Version:           1.0.0
 * Author:            Tash
 * License:           GPL-2.0+
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       thf_donate
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

function thf_login_form_shortcode( $atts, $content = null ) {
 
	extract( shortcode_atts( array(
      'redirect' => ''
      ), $atts ) );
 
	if (!is_user_logged_in()) {
		if($redirect) {
			$redirect_url = $redirect;
		} else {
			$redirect_url = get_permalink();
		}
		$redirect_url=site_url('/').'wp-admin/edit.php?post_type=thf_events';
		$form = wp_login_form(array('echo' => false, 'redirect' => $redirect_url ));
		
	}else{
		$form ='<p>Already Login</p>';
	} 
	return $form;
}
add_shortcode('thf_loginform', 'thf_login_form_shortcode');

add_shortcode( 'thf_custom_registration', 'thf_custom_registration_shortcode' );
 
// The callback function that will replace [book]
function thf_custom_registration_shortcode() {
    ob_start();
    thf_custom_registration_function();
    return ob_get_clean();
}
function thf_custom_registration_function() {
    if ( isset($_POST['submit'] ) && isset($_POST['email'])&& isset($_POST['password'])) {
        /*registration_validation(
        $_POST['username'],
        $_POST['password'],
        $_POST['email'],
        $_POST['website'],
        $_POST['fname'],
        $_POST['lname'],
        $_POST['nickname'],
        $_POST['bio']
        );*/
         
        // sanitize user form input
        global $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $website    =   esc_url( $_POST['website'] );
        $first_name =   sanitize_text_field( $_POST['fname'] );
        $last_name  =   sanitize_text_field( $_POST['lname'] );
        $nickname   =   sanitize_text_field( $_POST['nickname'] );
        $bio        =   esc_textarea( $_POST['bio'] );
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
        $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $nickname,
        $bio
        );
    }
 
    registration_form(
        $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $nickname,
        $bio
        );
}
function complete_registration() {
	
    global $reg_errors, $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
    
        $userdata = array(
        'user_login'    =>   $username,
        'user_email'    =>   $email,
        'user_pass'     =>   $password,
        'user_url'      =>   $website,
        'first_name'    =>   $first_name,
        'last_name'     =>   $last_name,
        'nickname'      =>   $nickname,
        'description'   =>   $bio,
		'role'	        =>   'school',
        );
        $user = wp_insert_user( $userdata );
        echo 'Registration complete. Go to <a href="' . get_site_url() . '/login">login page</a>.';   
    
}

function registration_form( $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio ) {
    echo '
    <style>
    div {
        margin-bottom:2px;
    }
     
    input{
        margin-bottom:4px;
    }
    </style>
    ';
 
    echo '<div class="registration_form">
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
    <div>
    <label for="username">Username <strong>*</strong></label>
    <input type="text" name="username" value="' . ( isset( $_POST['username'] ) ? $username : null ) . '">
    </div>
     
    <div>
    <label for="password">Password <strong>*</strong></label>
    <input type="password" name="password" value="' . ( isset( $_POST['password'] ) ? $password : null ) . '">
    </div>
     
    <div>
    <label for="email">Email <strong>*</strong></label>
    <input type="text" name="email" value="' . ( isset( $_POST['email']) ? $email : null ) . '">
    </div>
     
    <div>
    <label for="website">Website</label>
    <input type="text" name="website" value="' . ( isset( $_POST['website']) ? $website : null ) . '">
    </div>
     
    <div>
    <label for="firstname">First Name</label>
    <input type="text" name="fname" value="' . ( isset( $_POST['fname']) ? $first_name : null ) . '">
    </div>
     
    <div>
    <label for="website">Last Name</label>
    <input type="text" name="lname" value="' . ( isset( $_POST['lname']) ? $last_name : null ) . '">
    </div>
     
    <div>
    <label for="nickname">School Name</label>
    <input type="text" name="nickname" value="' . ( isset( $_POST['nickname']) ? $nickname : null ) . '">
    </div>
     
    <div>
    <label for="bio">About / Bio</label>
    <textarea name="bio">' . ( isset( $_POST['bio']) ? $bio : null ) . '</textarea>
    </div>
    <div class="regist_sub"><span><input type="submit" name="submit"  value="Register"/></span></div>
    </form></div>
    ';
}

function func_create_event_post_type(){
		$labels = array(
			'name'                  => _x( 'Events', 'Post Type General Name', 'thf_donate' ),
			'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'thf_donate' ),
			'menu_name'             => __( 'Events', 'thf_donate' ),
			'name_admin_bar'        => __( 'Events', 'thf_donate' ),
			'archives'              => __( 'Events Archives', 'thf_donate' ),
			'parent_item_colon'     => __( 'Parent Events:', 'thf_donate' ),
			'all_items'             => __( 'Events', 'thf_donate' ),
			'add_new_item'          => __( 'Add Event', 'thf_donate' ),
			'add_new'               => __( 'Add Event', 'thf_donate' ),
			'new_item'              => __( 'New Event', 'thf_donate' ),
			'edit_item'             => __( 'Edit Event', 'thf_donate' ),
			'update_item'           => __( 'Update Event', 'thf_donate' ),
			'view_item'             => __( 'View Event', 'thf_donate' ),
			'search_items'          => __( 'Search Events', 'thf_donate' ),
			'not_found'             => __( 'Not found', 'thf_donate' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'thf_donate' ),
			'featured_image'        => __( 'Featured Image', 'thf_donate' ),
			'set_featured_image'    => __( 'Set featured image', 'thf_donate' ),
			'remove_featured_image' => __( 'Remove featured image', 'thf_donate' ),
			'use_featured_image'    => __( 'Use as featured image', 'thf_donate' ),
			'insert_into_item'      => __( 'Insert into Events', 'thf_donate' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Events', 'thf_donate' ),
			'items_list'            => __( 'Events list', 'thf_donate' ),
			'items_list_navigation' => __( 'Events list navigation', 'thf_donate' ),
			'filter_items_list'     => __( 'Filter Events list', 'thf_donate' ),
		);
		$args = array(
			'label'                 => __( 'Events', 'thf_donate' ),
			'description'           => __( 'Add Event', 'thf_donate' ),
			'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
			'labels'                => $labels,
			//'taxonomies'            => array( 'category', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_nav_menus' => false,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			
			
		);
		register_post_type( 'thf_events', $args );
		
}	

add_action( "init", "func_create_event_post_type" );

function func_create_programme_post_type(){
		$labels = array(
			'name'                  => _x( 'Programme', 'Post Type General Name', 'thf_donate' ),
			'singular_name'         => _x( 'Programme', 'Post Type Singular Name', 'thf_donate' ),
			'menu_name'             => __( 'Programmes', 'thf_donate' ),
			'name_admin_bar'        => __( 'Programmes', 'thf_donate' ),
			'archives'              => __( 'Programmes Archives', 'thf_donate' ),
			'parent_item_colon'     => __( 'Parent Programmes:', 'thf_donate' ),
			'all_items'             => __( 'Programmes', 'thf_donate' ),
			'add_new_item'          => __( 'Add Programme', 'thf_donate' ),
			'add_new'               => __( 'Add Programme', 'thf_donate' ),
			'new_item'              => __( 'New Programme', 'thf_donate' ),
			'edit_item'             => __( 'Edit Programme', 'thf_donate' ),
			'update_item'           => __( 'Update Programme', 'thf_donate' ),
			'view_item'             => __( 'View Programme', 'thf_donate' ),
			'search_items'          => __( 'Search Programmes', 'thf_donate' ),
			'not_found'             => __( 'Not found', 'thf_donate' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'thf_donate' ),
			'featured_image'        => __( 'Featured Image', 'thf_donate' ),
			'set_featured_image'    => __( 'Set featured image', 'thf_donate' ),
			'remove_featured_image' => __( 'Remove featured image', 'thf_donate' ),
			'use_featured_image'    => __( 'Use as featured image', 'thf_donate' ),
			'insert_into_item'      => __( 'Insert into Programmes', 'thf_donate' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Programmes', 'thf_donate' ),
			'items_list'            => __( 'Programmes list', 'thf_donate' ),
			'items_list_navigation' => __( 'Programmes list navigation', 'thf_donate' ),
			'filter_items_list'     => __( 'Filter Programmes list', 'thf_donate' ),
		);
		$args = array(
			'label'                 => __( 'Programmes', 'thf_donate' ),
			'description'           => __( 'Add Programme', 'thf_donate' ),
			'supports'				=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'), 
			'labels'                => $labels,
			//'taxonomies'            => array( 'category', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_nav_menus' => false,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			
			
		);
		register_post_type( 'thf_programme', $args );
		
}	

add_action( "init", "func_create_programme_post_type" );


function posts_for_current_author($query) {
    global $pagenow;
	
    if( 'edit.php' != $pagenow || !$query->is_admin )
        return $query;
    $user = wp_get_current_user();
		  
    if( (in_array( 'school', (array) $user->roles ) || in_array( 'partner', (array) $user->roles )) && isset($_GET["page"])!="thf-requests" &&(isset($_GET["post_type"])=="thf_events"||isset($_GET["post_type"])=="thf_programme")) {
        global $user_ID;
       $query->set('author', $user_ID );
    }
    return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

add_action('add_meta_boxes','thf_events_add_custom_box');

function thf_events_add_custom_box(){
	add_meta_box(
	        'thf_events_cust_box_id',           // Unique ID
	        'Events Options',  // Box title
	        'thf_events_cust_box_html',  // Content callback, must be of type callable
	        'thf_events'                  // Post type
	    );
}

function thf_events_cust_box_html($post){
	$event_programme = get_post_meta( $post->ID,'event_programme',true );
	$event_date = get_post_meta( $post->ID,'event_date',true );
	$event_emails_data = get_post_meta( $post->ID,'event_emails_data',true );
	$beneficiary_schools_emails = get_post_meta( $post->ID,'beneficiary_schools_emails',true );
	/*if(!empty($beneficiary_schools_emails)){
		$beneficiary_schools_emails=implode(",",$beneficiary_schools_emails);
	}*/
	$beneficiary_approved= get_post_meta( $post->ID,'beneficiary_approved',true );
	$partner_approved=get_post_meta( $post->ID, 'partner_approved',true);
	$admin_approved=get_post_meta( $post->ID, 'admin_approved',true);
	
	$event_programme_partner=get_post_meta( $post->ID, 'event_programme_partner',true);
	
	?>
<style>
.event_thf_sec table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.event_thf_sec td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

.event_thf_sec tr:nth-child(even) {
  background-color: #dddddd;
}
.event_thf_sec input[type=text],.event_thf_sec select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

.event_thf_sec label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

.event_thf_sec input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

.event_thf_sec input[type=submit]:hover {
  background-color: #45a049;
}

.event_thf_sec .container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.event_thf_sec .col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.event_thf_sec .col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.event_thf_sec .row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .event_thf_sec .col-25, .event_thf_sec .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"  />
<div class="event_thf_sec">

	<span>
		<label for="event_programme">Select Programme</label><br/>
		<select name="event_programme" id="event_programme" class="postbox">
			<option value="">Select Programme...</option>
			<?php
				$args=array(
				'post_type'=>'thf_programme',
				'post_status'=>'publish',
				'posts_per_page' =>-1
			);
			$prog_query=new wp_Query($args);
			if(!empty($prog_query->posts)){
				foreach($prog_query->posts as $prog_p){
					$selec='';
					if($event_programme==$prog_p->ID){
						$selec='selected';
					}
					echo '<option value="'.$prog_p->ID.'"  data-author="'.$prog_p->post_author.'"   '.$selec.'>'.$prog_p->post_title.'</option>';
				}
			}
			?>
		</select>
		<input type="hidden" name="event_programme_partner" id="event_programme_partner" value="<?php echo $event_programme_partner;?>" />
		<a href="/thf_programme/" target="_blank">Click Here to view Details of programmes</a>
    </span><br/>
	<span>
		<label for="event_programme">Select Date</label><br/>
		<input type="text" name="event_date" id="event_date" class="postbox" value="<?php echo $event_date; ?>" />
    </span><br/>
	<span>
		<label for="event_programme">Beneficiary Schools Email(Please separate email addresses with a comma)</label><br/>
		<textarea name="beneficiary_schools_emails" id="beneficiary_schools_emails" class="postbox" ><?php echo $beneficiary_schools_emails; ?></textarea>	
    </span><br/>
	<span>
		<label for="over_all">Overall Status:</label>
		<?php 
			if($beneficiary_approved==1&&$partner_approved==1&&$admin_approved==1){
				echo 'Approved';
			}else{
				echo 'Pending';
			}
		
		?>
    </span><br/>
	<span>
		<label for="beneficiary_approved">Beneficiaries Status:</label>
		<?php 
			if($beneficiary_approved==1){
				echo 'Approved';
			}else{
				echo 'Pending';
			}
		
		?>
    </span><br/>
	<span>
		<label for="beneficiary_approved">Partner Status:</label>
		<?php 
	        if($partner_approved==-1){
				$partner_request_date=get_post_meta( $post->ID, 'partner_request_date',true);
				echo 'Change Date Request('.$partner_request_date.')';
			}elseif($partner_approved==1){
				echo 'Approved';
			}else{
				echo 'Pending';
			}
		
		?>
    </span><br/>
	<span>
		<label for="beneficiary_approved">Henman Foundation Status:</label>
		<?php 
			if($admin_approved==-1){
				$admin_request_date=get_post_meta( $post->ID, 'admin_request_date',true);
				echo 'Change Date Request('.$admin_request_date.')';
			}elseif($admin_approved==1){
				echo 'Approved';
			}else{
				echo 'Pending';
			}
		
		?>
    </span><br/>
	<h3>Beneficiary Statuses</h3>
	<table>
  <tr>
    <th>Email</th>
    <th>Date</th>
    <th>Status</th>
  </tr>
	<?php 
		if(!empty($event_emails_data)){
			$event_emails_data=json_decode($event_emails_data,true);
			
			foreach($event_emails_data as $key=>$data){
				if($data["status"]==0){
					$status="Pending";
				}
				if($data["status"]==1){
					$status="Approved";
				}
				if($data["status"]==-1){
					$status="Change Date request";
				}
				echo '<tr>
					<td>'.$key.'</td>
					<td>'.$data["date"].'</td>
					<td>'.$status.'</td>
				  </tr>';
			}
		}
		
		?>	
 
  
</table>
</div>	


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
	<script>
	jQuery(document).ready(function(){
		jQuery("#event_date").datetimepicker({
  format: 'd-m-Y H:i'
});
		jQuery("#event_programme").on("change",function(){
			var event_programme_partner=jQuery("#event_programme option:selected").attr("data-author");
			jQuery("#event_programme_partner").val(event_programme_partner);
		});
	});
	</script>

	<?php
}
//print_r(get_option("demo"));
function thf_reports_meta_box_save($post_id){

	
		if( isset( $_POST['event_programme'] ) ){
			update_post_meta( $post_id, 'event_programme', wp_kses( $_POST['event_programme'], $allowed ) );
	    }
	
	   if( isset( $_POST['event_programme_partner'] ) ){
			update_post_meta( $post_id, 'event_programme_partner', wp_kses( $_POST['event_programme_partner'], $allowed ) );
	    }
	//update_option("demo",$_POST);
	
	    if( isset( $_POST['event_date'] ) ){
			update_post_meta( $post_id, 'event_date', wp_kses( $_POST['event_date'], $allowed ) );
			update_post_meta( $post_id, 'event_date_strtotime', wp_kses( strtotime($_POST['event_date']), $allowed ) );
	    }
	  if( isset( $_POST['beneficiary_schools_emails'] ) ){
		    $beneficiary_schools_emails=$_POST["beneficiary_schools_emails"];
		  update_post_meta( $post_id, 'beneficiary_schools_emails',  $beneficiary_schools_emails);
		    $beneficiary_schools_emails=explode(",",$beneficiary_schools_emails);
		    
		    $json_emails=[];
		    $beneficiary_approved=0;
		    $event_emails_data=get_post_meta( $post_id, 'event_emails_data', true);
		 	$event_created_email=get_option("event_created_email");
			$event_updated_email=get_option("event_updated_email");
		    if(!empty($event_emails_data)){
				$event_emails_data=json_decode($event_emails_data,true);
				$body=$event_updated_email;
			}else{
				$event_emails_data=[];
				$body=$event_created_email;
			}
			  $body=str_replace("%event%", get_the_title($post_id) , $body);
			  $body=str_replace("%programme%", get_the_title($_POST['event_programme']) , $body);
			  $body=str_replace("%date%", $_POST['event_date'] , $body);
			  $body=str_replace("%user%", $user->user_email , $body);
		  
		  $headers = array('Content-Type: text/html; charset=UTF-8','From:THF<admin@thf.techzir.com>');
		    if(!empty($beneficiary_schools_emails)){ $beneficiary_approved=1;
				foreach($beneficiary_schools_emails as $emails){
					$emails = preg_replace('/\s+/', '', $emails);
					if(isset($event_emails_data[$emails])){
						$json_emails[$emails]["status"]= $event_emails_data[$emails]["status"];
						$json_emails[$emails]["date"]= $event_emails_data[$emails]["date"];
					}else{
						$json_emails[$emails]["status"]= 0;
						$json_emails[$emails]["date"]= $_POST['event_date'];
					}
					// 
					if($json_emails[$emails]["status"]!=1){
						$beneficiary_approved=0;
					}else{
						 wp_mail( $emails,'THF Event', $body, $headers );
					}
				}
			}
		  	update_post_meta( $post_id, 'beneficiary_approved', $beneficiary_approved);	
		    $partner_approved=get_post_meta( $post_id, 'partner_approved',true);
		    if($partner_approved!=1&&!empty($_POST["event_programme_partner"])){
				$partner_data=get_userdata( $_POST["event_programme_partner"] );
				
				wp_mail( $partner_data->user_email,'THF Event', $body, $headers );
			}
			update_post_meta( $post_id, 'event_emails_data', json_encode($json_emails));
	    }
	   
	
	
}	
add_action( 'save_post','thf_reports_meta_box_save');



function remove_those_menu_items( $menu_order ){
            global $menu;
	      
            $user = wp_get_current_user();
		   if ( in_array( 'school', (array) $user->roles ) ) {
			   remove_menu_page( 'tools.php' );
			   remove_menu_page( 'edit.php' );
			   remove_menu_page( 'edit-comments.php' );
			   remove_menu_page( 'edit.php?post_type=thf_programme' );
		   }
	 
		   if ( in_array( 'partner', (array) $user->roles ) ) {
			   remove_menu_page( 'tools.php' );
			   remove_menu_page( 'edit.php' );
			   remove_menu_page( 'edit-comments.php' );
		   }
            
    }

 //Then just Hook that function to "menu_order"
add_action( 'admin_init', 'remove_those_menu_items' );
function func_admin_menu_thf_requests(){
	add_submenu_page(
				'edit.php?post_type=thf_events',
				__( 'Requests', 'menu-test' ),
				__( 'Requests', 'menu-test' ),
				'see_thf_requests',
				'thf-requests',
				'func_admin_html_thf_requests'
			);
	add_submenu_page(
				'edit.php?post_type=thf_events',
				__( 'Emails', 'menu-test' ),
				__( 'Emails', 'menu-test' ),
				'manage_options',
				'thf-requests-emails',
				'func_admin_html_thf_requests_emails'
			);
	
}	
add_action( "admin_menu", "func_admin_menu_thf_requests" );
function func_admin_html_thf_requests_emails(){
	?>
		<style>
			.setting_form .selectWrap {
				height: 38px;
				border-radius: 3px;
				overflow: hidden;
				border: 1px solid #7e8892;
				margin-right: 15px;
				max-width: 450px;
				width: 100%;
			}
			.setting_form .selectWrap .wp_select{
				cursor: pointer;
				height: 100%;
				width: 100%;
				border: 0;
				padding-left: 10px;
				background-color: #fff;
				color:  #6d6d6d;
				font-weight: 600;
				border-radius: inherit; 
			}
			.wp_select:focus {
				outline: 0;
				box-shadow: none;
			}
			.setting_form .form_group {
				display: flex;
				margin-bottom: 35px;
			}
			.setting_form .form_label {
				width: 240px;
				padding-right: 25px;
				color: #3c3c3c;
				font-weight: 600;
				font-size: 16px;
			}
			.setting_form .inputWrap {
				max-width: 450px;
				width: 100%;
			}
			.setting_form .order-notes-textbox {
				width: 100%;
				resize: none;
			}
			.setting_form .form_control:focus {
				outline: 0;
				box-shadow: none;
			}
			.setting_form .form_control {
				padding: 5px 10px;
				border: 1px solid #7e8892;
				height: 100px;
				border-radius: 3px;
				width: 100%;
			}
			.submit_btn {
				background-color: #b29c84;
				color: #fff;
				padding: 5px 22px;
				border: 1px solid #8d8d8d;
				border-radius: 2px;
				font-size: 13px;
				text-align: center;
				text-decoration: none;
				transition: all 0.35s ease;
			}
			.setting_form .submit_btn:hover {
				border-color: rgba(178, 156, 132, 0.85) !important;
				background-color: rgba(178, 156, 132, 0.85) !important;
			}
			.setting_form .form_group.text_center {
				text-align: center;
			}


			/*** Distance Fee Table **/
			.distanceFeeSection .sectionTitle {
				color: #3c3c3c;
				font-size: 18px;
				line-height: 18px;
				margin: 0 0 10px;
				font-weight: 600;
			}
			.dfTableWrap {
				padding: 25px 45px 15px 25px;
				border: 1px solid #7e8892;
				border-radius: 3px;
				display: block;
				width: 100%;
				overflow-x: auto;
				-webkit-overflow-scrolling: touch;
			}
			.dft_row {
				display: flex;
				align-items: center;
				min-width: 690px;
				margin-bottom: 15px;
			}
			.selectWrap {
				height: 35px;
				border-radius: 3px;
				overflow: hidden;
				border: 1px solid #7e8892;
				margin-right: 15px;
			}
			.dft_row .selectWrap:last-child {
				margin-right: 0;
			}
			.selectWrap .wp_select{
				cursor: pointer;
				height: 100%;
				width: 100%;
				border: 0;
				padding-left: 10px;
				background-color: #fff;
				color:  #6d6d6d;
				font-weight: 600;
				border-radius: inherit; 
			}
		</style>
		<?php 
	
			if(isset($_POST["emails_submit_btn"])){
				unset($_POST["emails_submit_btn"]);
				foreach($_POST as $key=>$value){
					update_option($key,stripslashes(wp_filter_post_kses($value)));
				}
			}
	
			$event_created_email=get_option("event_created_email");
			$event_updated_email=get_option("event_updated_email");
			$beneficiary_approved=get_option("beneficiary_approved");
			$beneficiary_onhold=get_option("beneficiary_onhold");
			$beneficiary_change_date=get_option("beneficiary_change_date");
			$partner_approved=get_option("partner_approved");
			$partner_onhold=get_option("partner_onhold");
			$partner_change_date=get_option("partner_change_date");
			$admin_approved=get_option("admin_approved");
			$admin_onhold=get_option("admin_onhold");
			$admin_change_date=get_option("admin_change_date");
	
		?>
		<div class="tabContent">
				<div class="tabPane fade active show" id="tab_01">
					<header class="hh_header">
						<h2>Emails</h2>
						<span>%event%  %programme%  %date% %user%</span>
					</header>
					<form action="<?php echo admin_url('/edit.php?post_type=thf_events&page=thf-requests-emails'); ?>" method="post" class="setting_form">
							<div class="form_group">
								<label class="form_label">Event Created</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="event_created_email"><?php echo $event_created_email; ?></textarea>
								</div>
							</div>
						    <div class="form_group">
								<label class="form_label">Event Updated</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="event_updated_email"><?php echo $event_updated_email; ?></textarea>
								</div>
							</div>
						
							<div class="form_group">
								<label class="form_label">Beneficiary Approved</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="beneficiary_approved"><?php echo $beneficiary_approved; ?></textarea>
								</div>
							</div>
							<div class="form_group">
								<label class="form_label">Beneficiary Onhold</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="beneficiary_onhold"><?php echo $beneficiary_onhold; ?></textarea>
								</div>
							</div>
							<div class="form_group">
								<label class="form_label">Beneficiary Change date</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="beneficiary_change_date" >
<?php echo $beneficiary_change_date; ?></textarea>
								</div>
							</div>
						
							<div class="form_group">
								<label class="form_label">Partner Approved</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="partner_approved" ><?php echo $partner_approved; ?></textarea>
								</div>
							</div>
							<div class="form_group">
								<label class="form_label">Partner Onhold</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="partner_onhold"><?php echo $partner_onhold; ?></textarea>
								</div>
							</div>
							<div class="form_group">
								<label class="form_label">Partner Change date</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="partner_change_date"><?php echo $partner_change_date; ?></textarea>
								</div>
							</div>
						
							<div class="form_group">
								<label class="form_label">Henman Foundation Approved</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="admin_approved"><?php echo $admin_approved; ?></textarea>
								</div>
							</div>
							<div class="form_group">
								<label class="form_label">Henman Foundation Onhold</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="admin_onhold"><?php echo $admin_onhold; ?></textarea>
								</div>
							</div>
							<div class="form_group">
								<label class="form_label">Henman Foundation Change date</label>
								<div class="inputWrap">
									<textarea class="form_control"  name="admin_change_date" ><?php echo $admin_change_date; ?></textarea>
								</div>
							</div>
							<div class="form_group text_center">
								<input type="submit" class="submit_btn" name="emails_submit_btn" >
							</div>
						</div>
					</form>
				</div>
			</div>
	<?php	
}

function func_admin_html_thf_requests(){
	$user = wp_get_current_user();
	if ( in_array( 'school', (array) $user->roles ) ) {
		func_get_school_requests($user);
	}
	if ( in_array( 'partner', (array) $user->roles ) ) {
		func_get_partner_requests($user);
	}
	if ( in_array( 'administrator', (array) $user->roles ) ) {
		func_get_admin_requests($user);
	}
	
	
	?>
	<?php
}
function func_get_school_requests($user){
	
	global $wpdb;
	$table=$wpdb->base_prefix."postmeta"; 
	$sql = "SELECT * FROM ".$table." WHERE `meta_key`='beneficiary_schools_emails' AND `meta_value` LIKE '%".$user->user_email."%'";
	$fetchdata = $wpdb->get_results($sql);
	
	?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"  />
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
		<table id="table_id" class="display">
			<thead>
				<tr>
					<th>Event</th>
					<th>Programme</th>
					<th>Host School</th>
					<th>Beneficiary Schools</th>
					<th>Date</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
	               if(isset($_GET["request_event"])&&isset($_GET["request_status"])){
					   $event_emails_data = get_post_meta(  $_GET["request_event"],'event_emails_data',true );
					   $event_emails_data =json_decode($event_emails_data,true);
					   $event_emails_data[$user->user_email]["status"]=$_GET["request_status"];
					   
						if($_GET["request_status"]==-1){
							 $event_emails_data[$user->user_email]["date"]=$_GET["request_date"];
						}
					   if(!empty($event_emails_data)){ $beneficiary_approved=1;
						   foreach($event_emails_data as $em_data){
							   if($em_data["status"]!=1){
								   $beneficiary_approved=0;
							   }
						   }
					   }
					   
					   update_post_meta( $_GET["request_event"], 'beneficiary_approved', $beneficiary_approved);	
					   update_post_meta( $_GET["request_event"], 'event_emails_data',json_encode($event_emails_data));
						
						$event_date = get_post_meta( $_GET["request_event"],'event_date',true );
					   
						$beneficiary_onhold=get_option("beneficiary_onhold");
						$beneficiary_change_date=get_option("beneficiary_change_date");
						$beneficiary_approved=get_option("beneficiary_approved");
					   
						$request_event=get_post($_GET["request_event"]);
						if($_GET["request_status"]==1){
							$body=$beneficiary_approved;
						}
						if($_GET["request_status"]==0){
							$body=$beneficiary_onhold;
						}
						if($_GET["request_status"]==-1){
							$body=$beneficiary_change_date;
						}
					  
					   $event_programme = get_post_meta( $request_event->ID,'event_programme',true );
					   $body=str_replace("%event%", $request_event->post_title , $body);
					   $body=str_replace("%date%", $event_date , $body);
					   $body=str_replace("%programme%", get_the_title($event_programme) , $body);
					   $body=str_replace("%user%", $user->user_email , $body);
					   
						$event_users=get_userdata($request_event->post_author);
						wp_mail( $event_user->user_email,'THF Event School Approval', $body, $headers );								
						
					}
				    if(!empty($fetchdata)){
						foreach($fetchdata as $data){
							$ppost=get_post($data->post_id);
							$event_programme = get_post_meta( $ppost->ID,'event_programme',true );
							$event_date = get_post_meta( $ppost->ID,'event_date',true );
							$event_emails_data = get_post_meta( $ppost->ID,'event_emails_data',true );
							$beneficiary_schools_emails = get_post_meta( $ppost->ID,'beneficiary_schools_emails',true );
							$event_emails_data =json_decode($event_emails_data,true);
							$authord=get_userdata($ppost->post_author);
							?>
								<tr>
									<td><?php echo $ppost->post_title; ?></td>
									<td><?php echo get_the_title($event_programme); ?></td>
									<td><?php echo $authord->display_name; ?></td>
									<td><?php echo $beneficiary_schools_emails; ?></td>
									<td><input type="text" class="event_date"  id="event_date_<?php echo $ppost->ID;  ?>" class="postbox" value="<?php echo $event_date; ?>" data-id="<?php echo $ppost->ID;  ?>" disabled/>   <?php   if($event_emails_data[$user->user_email]["status"]==-1){echo '<br/> Request Date:'.$event_emails_data[$user->user_email]["date"];}
										
										?>
									</td>
									<td><?php 
										if($event_emails_data[$user->user_email]["status"]!=1){
											echo '<a href="/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=1">Change to Approve</a> / ';
											echo '<a style="cursor:pointer;" class="change_gtn_date" data-id="'.$ppost->ID.'">Change Date</a>';
											echo '<a id="btn_date_'.$ppost->ID.'" href="'.site_url().'/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=-1&&request_date='.$event_date.'"  data-link="'.site_url().'/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=-1"  data-date="'.$event_date.'" style="display:none;cursor:pointer;">Request Date</a>';
										}else{
											echo '<a href="/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=0">Change to Reject Date</a>';
										}
										?></td>
								</tr>
							<?php
						}
					}
				?>
			</tbody>
		</table>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
			<script>
			jQuery(document).ready(function(){
				jQuery(".event_date").datetimepicker({
  format: 'd-m-Y H:i'
});
				jQuery(".change_gtn_date").on("click",function(){
					var dataid=jQuery(this).attr("data-id");
					jQuery(this).hide();
					jQuery("#btn_date_"+dataid).show();
					jQuery("#event_date_"+dataid).prop("disabled",false);
					
				});
				jQuery(".event_date").on("change",function(){
					
					var dataid=jQuery(this).attr("data-id");
					var values=jQuery(this).val();
					jQuery("#btn_date_"+dataid).attr("data-date",values);
					var datahref=jQuery("#btn_date_"+dataid).attr("data-link");
					//alert(datahref+"&&btn_date_="+values);
					jQuery("#btn_date_"+dataid).attr("href",datahref+"&&request_date="+values);
				});
			});
			</script>
      <script>
		jQuery(document).ready( function () {
			jQuery('#table_id').DataTable();
		} );
      </script>
	<?php
}

function func_get_partner_requests($user){
	
	$args=array(
				'post_type'=>'thf_events',
				'post_status'=>'publish',
				'posts_per_page' =>-1,
				'meta_query'        => array(
					array(
						'key'       => 'event_programme_partner',
						'value'     => $user->ID,
						'compare'   => '=',
					),
				),
				'orderby' 			=> 'date',
				'order' 			=> 'DESC',
			);
			$prog_query=new wp_Query($args);
	?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"  />
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
		<table id="table_id" class="display">
			<thead>
				<tr>
					<th>Event</th>
					<th>Programme</th>
					<th>Host School</th>
					<th>Beneficiary Schools</th>
					<th>Date</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
	                if(isset($_GET["request_event"])&&isset($_GET["request_status"])){
						update_post_meta( $_GET["request_event"], 'partner_approved',$_GET["request_status"]);
						if($_GET["request_status"]==-1){
							update_post_meta( $_GET["request_event"],'partner_request_date',$_GET["request_date"] );
						}
						
						$event_date = get_post_meta( $_GET["request_event"],'event_date',true );
						
						$partner_onhold=get_option("partner_onhold");
						$partner_change_date=get_option("partner_change_date");
						$partner_approved=get_option("partner_approved");
					   
						$request_event=get_post($_GET["request_event"]);
						if($_GET["request_status"]==1){
							$body=$partner_approved;
						}
						if($_GET["request_status"]==0){
							$body=$partner_onhold;
						}
						if($_GET["request_status"]==-1){
							$body=$partner_change_date;
						}
					  
					   $event_programme = get_post_meta( $request_event->ID,'event_programme',true );
					   $body=str_replace("%event%", $request_event->post_title , $body);
					   $body=str_replace("%date%", $event_date , $body);
					   $body=str_replace("%programme%", get_the_title($event_programme) , $body);
					   $body=str_replace("%user%", $user->user_email , $body);
												
						$event_users=get_userdata($request_event->post_author);
						wp_mail( $event_user->user_email,'THF Event Partner Approval', $body, $headers );								
						
					}
				    if(!empty($prog_query->posts)){
						foreach($prog_query->posts as $ppost){
							$event_programme = get_post_meta( $ppost->ID,'event_programme',true );
							$event_date = get_post_meta( $ppost->ID,'event_date',true );
							$partner_approved=get_post_meta( $ppost->ID, 'partner_approved',true);
							$partner_request_date=get_post_meta( $ppost->ID, 'partner_request_date',true);
							$beneficiary_schools_emails = get_post_meta( $ppost->ID,'beneficiary_schools_emails',true );

              $authord=get_userdata($ppost->post_author);
							?>
								<tr>
									<td><?php echo $ppost->post_title; ?></td>
									<td><?php echo get_the_title($event_programme); ?></td>
									<td><?php echo $authord->display_name; ?></td>
                  					<td><?php echo $beneficiary_schools_emails; ?></td>
									<td><input type="text" class="event_date"  id="event_date_<?php echo $ppost->ID;  ?>" class="postbox" value="<?php echo $event_date; ?>" data-id="<?php echo $ppost->ID;  ?>" disabled/>   <?php   if($partner_approved==-1){echo '<br/> Request Date:'.$partner_request_date;}
										
										?>
									</td>
									<td><?php 
										if($partner_approved!=1){
											echo '<a href="/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=1">Change to Approve</a> / ';
											echo '<a style="cursor:pointer;" class="change_gtn_date" data-id="'.$ppost->ID.'">Change Date</a>';
											echo '<a id="btn_date_'.$ppost->ID.'" href="'.site_url().'/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=-1&&request_date='.$event_date.'"  data-link="'.site_url().'/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=-1"  data-date="'.$event_date.'" style="display:none;cursor:pointer;">Request Date</a>';
										}else{
											echo '<a href="/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=0">Change to Reject date</a>';
										}
										?></td>
								</tr>
							<?php
						}
					}
				?>
			</tbody>
		</table>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
			<script>
			jQuery(document).ready(function(){
				jQuery(".event_date").datetimepicker({
  format: 'd-m-Y H:i'
});
				jQuery(".change_gtn_date").on("click",function(){
					var dataid=jQuery(this).attr("data-id");
					jQuery(this).hide();
					jQuery("#btn_date_"+dataid).show();
					jQuery("#event_date_"+dataid).prop("disabled",false);
					
				});
				jQuery(".event_date").on("change",function(){
					
					var dataid=jQuery(this).attr("data-id");
					var values=jQuery(this).val();
					jQuery("#btn_date_"+dataid).attr("data-date",values);
					var datahref=jQuery("#btn_date_"+dataid).attr("data-link");
					//alert(datahref+"&&btn_date_="+values);
					jQuery("#btn_date_"+dataid).attr("href",datahref+"&&request_date="+values);
				});
			});
			</script>
      <script>
		jQuery(document).ready( function () {
			jQuery('#table_id').DataTable();
		} );
      </script>
	<?php
	       
}

function func_get_admin_requests($user){
	
	$args=array(
				'post_type'=>'thf_events',
				'post_status'=>'publish',
				'posts_per_page' =>-1,
				'meta_query'        => array(
					'relation' => 'AND',
					array(
						'key'       => 'partner_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'beneficiary_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					
				),
				'orderby' 			=> 'date',
				'order' 			=> 'DESC',
			);
			$prog_query=new wp_Query($args);
	        ?>
			<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"  />
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
		<table id="table_id" class="display">
			<thead>
				<tr>
					<th>Event</th>
					<th>Programme</th>
					<th>Host School</th>
					<th>Beneficiary Schools</th>
					<th>Date</th>
					<th>Beneficiary Status</th>
					<th>Partner Status</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
	                if(isset($_GET["request_event"])&&isset($_GET["request_status"])){
						update_post_meta( $_GET["request_event"], 'admin_approved',$_GET["request_status"]);
						if($_GET["request_status"]==-1){
							update_post_meta( $_GET["request_event"],'admin_request_date',$_GET["request_date"] );
						}
						
						$event_date = get_post_meta( $_GET["request_event"],'event_date',true );
						$user=wp_get_current_user();
						
						$admin_onhold=get_option("admin_onhold");
						$admin_change_date=get_option("admin_change_date");
						$admin_approved=get_option("admin_approved");
					   
						$request_event=get_post($_GET["request_event"]);
						if($_GET["request_status"]==1){
							$body=$admin_approved;
						}
						if($_GET["request_status"]==0){
							$body=$admin_onhold;
						}
						if($_GET["request_status"]==-1){
							$body=$admin_change_date;
						}
					  
					   $event_programme = get_post_meta( $request_event->ID,'event_programme',true );
					   $body=str_replace("%event%", $request_event->post_title , $body);
					   $body=str_replace("%date%", $event_date , $body);
					   $body=str_replace("%programme%", get_the_title($event_programme) , $body);
					   $body=str_replace("%user%", $user->user_email , $body);
												
						$event_users=get_userdata($request_event->post_author);
						wp_mail( $event_user->user_email,'THF Henman Foundation Approval', $body, $headers );								
						
					}
				    if(!empty($prog_query->posts)){
						foreach($prog_query->posts as $ppost){
							$event_programme = get_post_meta( $ppost->ID,'event_programme',true );
							$event_date = get_post_meta( $ppost->ID,'event_date',true );
							$admin_approved=get_post_meta( $ppost->ID, 'admin_approved',true);
							$admin_request_date=get_post_meta( $ppost->ID, 'admin_request_date',true);
							$partner_approved=get_post_meta( $ppost->ID, 'partner_approved',true);
							$partner_request_date=get_post_meta( $ppost->ID, 'partner_request_date',true);
							$beneficiary_approved=get_post_meta( $ppost->ID, 'beneficiary_approved',true);
							$beneficiary_schools_emails = get_post_meta( $ppost->ID,'beneficiary_schools_emails',true );
          
              $authord=get_userdata($ppost->post_author);
							?>
								<tr>
									<td><?php echo $ppost->post_title; ?></td>
									<td><?php echo get_the_title($event_programme); ?></td>
									<td><?php echo $authord->display_name; ?></td>
                  <td><?php echo $beneficiary_schools_emails; ?></td>
									<td><input type="text" class="event_date"  id="event_date_<?php echo $ppost->ID;  ?>" class="postbox" value="<?php echo $event_date; ?>" data-id="<?php echo $ppost->ID;  ?>" disabled/>   <?php   if($admin_approved==-1){echo '<br/> Request Date:'.$admin_request_date;}
										
										?>
									</td>
									<td>
									<?php 
										if($beneficiary_approved==1){
											echo 'Approved';
										}else{
											echo 'Pending';
										}

									?>
								</td>
								<td>
									<?php 
										if($partner_approved==-1){
											$partner_request_date=get_post_meta( $post->ID, 'partner_request_date',true);
											echo 'Change Date Request('.$partner_request_date.')';
										}elseif($partner_approved==1){
											echo 'Approved';
										}else{
											echo 'Pending';
										}

									?>
								</td>
									<td><?php 
										if($admin_approved!=1){
											echo '<a href="/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=1">Change to Approve</a> / ';
											echo '<a style="cursor:pointer;" class="change_gtn_date" data-id="'.$ppost->ID.'">Change Date</a>';
											echo '<a id="btn_date_'.$ppost->ID.'" href="'.site_url().'/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=-1&&request_date='.$event_date.'"  data-link="'.site_url().'/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=-1"  data-date="'.$event_date.'" style="display:none;cursor:pointer;">Request Date</a>';
										}else{
											echo '<a href="/wp-admin/edit.php?post_type=thf_events&page=thf-requests&request_event='.$ppost->ID.'&&request_status=0">Change to Reject Date</a>';
										}
										?></td>
								</tr>
							<?php
						}
					}
				?>
			</tbody>
		</table>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
			<script>
			jQuery(document).ready(function(){
				jQuery(".event_date").datetimepicker({
  format: 'd-m-Y H:i'
});
				jQuery(".change_gtn_date").on("click",function(){
					var dataid=jQuery(this).attr("data-id");
					jQuery(this).hide();
					jQuery("#btn_date_"+dataid).show();
					jQuery("#event_date_"+dataid).prop("disabled",false);
					
				});
				jQuery(".event_date").on("change",function(){
					
					var dataid=jQuery(this).attr("data-id");
					var values=jQuery(this).val();
					jQuery("#btn_date_"+dataid).attr("data-date",values);
					var datahref=jQuery("#btn_date_"+dataid).attr("data-link");
					//alert(datahref+"&&btn_date_="+values);
					jQuery("#btn_date_"+dataid).attr("href",datahref+"&&request_date="+values);
				});
			});
			</script>
      <script>
		jQuery(document).ready( function () {
			jQuery('#table_id').DataTable();
		} );
      </script>
            <?php
}


add_shortcode( 'thf_custom_calendar', 'thf_custom_calendar_view' );
 
// The callback function that will replace [book]
function thf_custom_calendar_view() {
    ob_start();
	
	global $post;
	$page_link=get_permalink($post->ID);
	if(isset($_GET["partner"])&&!empty($_GET["partner"])&&isset($_GET["programme"])&&!empty($_GET["programme"])){
		$args=array(
				'post_type'=>'thf_events',
				'post_status'=>'publish',
				'posts_per_page' =>-1,
				'meta_key'          => 'event_date_strtotime',
  				'orderby'           => 'meta_value_num',
  				'order'             => 'ASC',
				'meta_query'        => array(
					'relation' => 'AND',
					array(
						'key'       => 'event_programme',
						'value'     => $_GET["programme"],
						'compare'   => '=',
					),
					array(
						'key'       => 'event_programme_partner',
						'value'     => $_GET["partner"],
						'compare'   => '=',
					),
					array(
						'key'       => 'partner_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'beneficiary_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'admin_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					
				),
				
			);
	}elseif(isset($_GET["partner"])&&!empty($_GET["partner"])){
		$args=array(
				'post_type'=>'thf_events',
				'post_status'=>'publish',
				'posts_per_page' =>-1,
				'meta_key'          => 'event_date_strtotime',
  				'orderby'           => 'meta_value_num',
  				'order'             => 'ASC',
				'meta_query'        => array(
					'relation' => 'AND',
					array(
						'key'       => 'event_programme_partner',
						'value'     => $_GET["partner"],
						'compare'   => '=',
					),
					array(
						'key'       => 'partner_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'beneficiary_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'admin_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					
				),
				
			);
	}elseif(isset($_GET["programme"])&&!empty($_GET["programme"])){
		$args=array(
				'post_type'=>'thf_events',
				'post_status'=>'publish',
				'posts_per_page' =>-1,
				'meta_key'          => 'event_date_strtotime',
  				'orderby'           => 'meta_value_num',
  				'order'             => 'ASC',
				'meta_query'        => array(
					'relation' => 'AND',
					array(
						'key'       => 'event_programme',
						'value'     => $_GET["programme"],
						'compare'   => '=',
					),
					array(
						'key'       => 'partner_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'beneficiary_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'admin_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					
				),
				
			);
	}else{
		$args=array(
				'post_type'=>'thf_events',
				'post_status'=>'publish',
				'posts_per_page' =>-1,
				'meta_key'          => 'event_date_strtotime',
  				'orderby'           => 'meta_value_num',
  				'order'             => 'ASC',
				'meta_query'        => array(
					'relation' => 'AND',
					array(
						'key'       => 'partner_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'beneficiary_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					array(
						'key'       => 'admin_approved',
						'value'     => 1,
						'compare'   => '=',
					),
					
				),
				
			);
		
	}
	
	if(isset($_GET["from_date"])&&!empty($_GET["from_date"])){
	
		

		$count_date=count($args['meta_query']);
		$args['meta_query'][$count_date]=array(
			'key' => 'event_date_strtotime',
			'value' =>strtotime(date("d-m-Y H:i",strtotime($_GET["from_date"]))),
			'compare' => '>=',
		);
	}
	
	
	if(isset($_GET["to_date"])&&!empty($_GET["to_date"])){
		$args['meta_query'][$count_date]=array(
			'key' => 'event_date_strtotime',
			'value' => strtotime(date("d-m-Y H:i",strtotime($_GET["to_date"]))),
			'compare' => '<=',
		);
	}
	if ( get_query_var('paged') ) {

		$paged = get_query_var('paged');

	} elseif ( get_query_var('page') ) {

		$paged = get_query_var('page');

	} else {

		$paged = 1;

	}
	
	$args["posts_per_page"]=5;
    $args["paged"]=$paged;

    //print_r($args);

	$prog_query=new wp_Query($args);
	
	
	       /* $js_array=[];
	        if(!empty($prog_query->posts)){ $temp=0;
				foreach($prog_query->posts as $ppost){
					$js_array[$temp]["title"]=$ppost->post_title;
					$js_array[$temp]["start"] = get_post_meta( $ppost->ID,'event_date',true );
					$temp++;
				}	
			}
	    //  $js_array=json_encode($js_array);*/
	       
    ?>
	
	<div class="thf_container">
		<div class="containerWrap">
			<div class="rowWrap">
				<div class="colWrap widhSMtwenty widthMDthirty">
					<div class="sideBar">
						<form action="" class="filters_aa">
							<div class="formRow">
								<label class="fLable">Date:</label>
								<div class="dFlex">
									<div class="fWrap">
										<span class="fTitle">From</span>
									   <input type="date" id="from_date" value="<?php if(isset($_GET["from_date"])){ echo $_GET["from_date"]; } ?>" class="formControl">
									</div>
									<div class="fWrap">
										<span class="fTitle">To</span>
										<input type="date" id="to_date" value="<?php if(isset($_GET["to_date"])){ echo $_GET["to_date"]; } ?>" class="formControl">
									</div>
								</div>
							</div>
							<div class="formRow">
								<label class="fLable">Programme:</label>
								<div class="selectWrap">
									<select name="programme" id="programme" class="fSelect">
										<option value=''>Select Programme</option>
										<?php 
											$args2=array(
												'post_type'=>'thf_programme',
												'post_status'=>'publish',
												'posts_per_page' =>-1
											);
											$prog_query2=new wp_Query($args2);
											if(!empty($prog_query2->posts)){
												foreach($prog_query2->posts as $query){
													$selec='';
													if(isset($_GET["programme"])&&$_GET["programme"]==$query->ID){ $selec='selected'; }
													echo '<option value="'.$query->ID.'"  '.$selec.' >'.$query->post_title.'</option>';
												}
											}
										?>
										
									</select>
								</div>
							</div>
							<div class="formRow">
								<label class="fLable">Partner:</label>
								<div class="selectWrap">
									<select name="partner" id="partner" class="fSelect">
										<option value=''>Select Partner</option>
										<?php
											$args3 = array(
												'role'    => 'partner',
												'orderby' => 'user_nicename',
												'order'   => 'ASC'
											);
											$users = get_users( $args3 );
											if(!empty($users)){
												foreach($users as $user){
													$selec='';
													if(isset($_GET["partner"])&&$_GET["partner"]==$user->ID){ $selec='selected'; }
													echo '<option value="'.$user->ID.'"  '.$selec.'>'.$user->user_login.'</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="colWrap widhSMeighty widthMDseventy">
					<div class="contHolder">
						<ul class="programmeList">
							<?php 
								 if(!empty($prog_query->posts)){ $temp=0;
									foreach($prog_query->posts as $ppost){
										//echo get_post_meta( $ppost->ID,'event_date_strtotime',true );
										$event_programme = get_post_meta($ppost->ID,'event_programme',true );
										$event_date = get_post_meta( $ppost->ID,'event_date',true );
										$event_programme_partner=get_post_meta( $ppost->ID, 'event_programme_partner',true);
										
										$host_user=get_userdata($ppost->post_author);
										$event_programme_partner=get_userdata($event_programme_partner);
										echo '<li>
												<div class="progWrap">
													<div class="dateWrap">
														'.date("d M Y", strtotime($event_date)).'
														
													</div>
													<div class="detailWrap">
														<div class="rrRow">
															<span class="txtLeft">Programme:</span>
															<span class="txtRight">'.get_the_title($event_programme).'</span>
														</div>
														<div class="rrRow">
															<span class="txtLeft">Partner:</span>
															<span class="txtRight">'.$event_programme_partner->display_name.'</span>
														</div>
														<div class="rrRow">
															<span class="txtLeft">Host School:</span>
															<span class="txtRight">'.$host_user->display_name.'</span>
														</div>
													</div>
												</div>
											</li>';
									}	
								}
							?>
							
							
							
						</ul>
					</div>
					<nav class="pag_btns">
						<span class="load_old"><?php previous_posts_link('Load Previous',$prog_query->max_num_pages) ?></span>
						<span class="load_new"><?php next_posts_link('Load More',$prog_query->max_num_pages) ?></span>
					</nav>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		 <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.css"/>
		 <div id='calendar'></div>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.js"></script>-->
		
		<script>
			jQuery(document).ready(function(){ 
				jQuery("#programme").on("change",function(){
					redirect_link_thf();	
				});
				jQuery("#partner").on("change",function(){
					redirect_link_thf();	
				});
				jQuery("#from_date").on("change",function(){
					redirect_link_thf();	
				});
				jQuery("#to_date").on("change",function(){
					redirect_link_thf();	
				});
				function redirect_link_thf(){
					var page_link='<?php echo $page_link; ?>';
					var partner=jQuery("#partner").val();
					var programme=jQuery("#programme").val();
					var from_date=jQuery("#from_date").val();
					var to_date=jQuery("#to_date").val();
					window.location.href = page_link+'?partner='+partner+'&programme='+programme+'&from_date='+from_date+'&to_date='+to_date;
				}
			});
		</script>
		<?php /* ?>
		<script>

$(function() {

    var todayDate = moment().startOf('day');
    var YM = todayDate.format('YYYY-MM');
    var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
    var TODAY = todayDate.format('YYYY-MM-DD');
    var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

    var cal = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month'
        },
        editable: true,
        eventLimit: 0, // allow "more" link when too many events
        navLinks: true,
        events: <?php// echo //$js_array; ?>,
    dayRender :function(a){
      //console.log(a)
    }
    });
  
});
  </script> <?php */?>
	</div>	

	<?php
    return ob_get_clean();
}


add_filter( 'manage_thf_events_posts_columns', 'custom_edit_thf_events_columns' );

function custom_edit_thf_events_columns($columns){
	 $columns['beneficiary_approved'] = __( 'Beneficiaries Status', 'your_text_domain' );
	 $columns['partner_approved'] = __( 'Partner Status', 'your_text_domain' );
	 $columns['admin_approved'] = __( 'Henman Foundation Status', 'your_text_domain' );
	 return $columns;
}
add_action( 'manage_thf_events_posts_custom_column' , 'custom_thf_events_column', 10, 2 );
function custom_thf_events_column( $column, $post_id ) {
	switch ( $column ) {
		case 'beneficiary_approved' :
			$beneficiary_approved=get_post_meta($post_id,"beneficiary_approved",true);
			if($beneficiary_approved==1){
					echo 'Approved';
				}else{
					echo 'Pending';
				}	
			break;
		case 'partner_approved' :
			$partner_approved=get_post_meta($post_id,"partner_approved",true);
			if($partner_approved==-1){
				$partner_request_date=get_post_meta( $post_id, 'partner_request_date',true);
				echo 'Change Date Request('.$partner_request_date.')';
			}elseif($partner_approved==1){
				echo 'Approved';
			}else{
				echo 'Pending';
			}
			break;
		case 'admin_approved' :
			$admin_approved=get_post_meta($post_id,"admin_approved",true);
			if($admin_approved==-1){
				$admin_request_date=get_post_meta( $post_id, 'admin_request_date',true);
				echo 'Change Date Request('.$admin_request_date.')';
			}elseif($admin_approved==1){
				echo 'Approved';
			}else{
				echo 'Pending';
			}
			break;
	}
}
function thf_acf_blocks() {

    wp_enqueue_style( 'thf-block-css', plugin_dir_url( __FILE__ ) .'thf-block-public.css' );


}

add_action( 'wp_enqueue_scripts', 'thf_acf_blocks' );

function thf_event_func_js_admin(){
	
	?>
	<script>
		jQuery(document).ready(function(){
			if(jQuery(".post-type-thf_events").length==1){
			
				if(jQuery("#title").text()==""){
					jQuery(".wp-heading-inline").after("<p style='color:red !important;'>Event Name Required</p>");	
				}
				
			}
		});
    </script>
	<?php
}

add_action("admin_footer","thf_event_func_js_admin");

