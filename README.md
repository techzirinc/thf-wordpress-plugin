## THF WordPress Plugin

The plugin was developed for **Tim Henman Foundation** on July 2021.

---

## Feature List

The plugin works by creating multiple user roles and each user role has their own set of capabilites. The plugin also creates two custom post types, **Events** and **Programmes**. Each of the user and their respective capabilites are described below.

### Schools

1. Can register their account through the registration form that the plugin creates when installed and activated.
2. Can login through a custom login form created by the plugin.
3. Schools can create, edit events. They can only see their own events.

### Partners

1. Can register their account through the registration form that the plugin creates when installed and activated.
2. Can login through a custom login form created by the plugin.
3. Partners can create, edit events as well as programs. They can only see their own events and programs that they have created.

### Admin

1. Has the access of the entire system. This is the default WordPress admin user role.
2. Add new host schools and partners to portal.
3. See all dates which are currently being proposed and what status/level of approval they are.

### Events

The events can be created by all the user roles. When a user creates a event, they are the host for that event. They can add the event description, event date, select a program to add to the event and add beneficiaries to the event.

### Programs

Programs can be created in the backend by Partner user role. This can later be added to the events.

### Flow

When an event is created either by a School or a Partner, the beneficaires are notified through email that they have been invited to the event. They have the option to accept or reject the invite and this can be done from their dashboard on the website. if they already have an account they are asked to login and approve, if not they are prompted to sign up.

If they reject the invite, they have the option to suggest alternate dates for conducting the event to the event creator. If they accept the invite, an email is generated and sent to super admin for final approval.

Once the super admin approves the event, it gets published and can be seen on the front end calendar present in one of the pages of the website. The calendar can be added to the website through a short code on any page of the website.

---